# ZulipEmojiSet


- emojiSetNCU - Emoji with Name and Code as Unicode (Int)

- emojiSetCNCU - Emoji with Category, Name and Code as Unicode (Int)


- emojiSetNCS - Emoji with Name and Code as String

- emojiSetCNCS - Emoji with Category, Name and Code as String
